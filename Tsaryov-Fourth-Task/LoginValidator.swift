//
//  LoginValidator.swift
//  Tsaryov-Fourth-Task
//
//  Created by Ivan Tsaryov on 15/01/2018.
//  Copyright © 2018 Ivan Tsaryov. All rights reserved.
//

import Foundation


class LoginValidator {
    
    func isLoginValid(_ login: String) -> Bool {
        let emailRegEx = "([a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z0-9]{1,})"
        let loginRegEx = "([a-zA-Z0-9.-])"
        
        let regEx = "(\(emailRegEx)|\(loginRegEx)){3,32}"
        
        print("Final regex: \(regEx)")
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", regEx)
        return emailTest.evaluate(with: login)
    }
    
}
