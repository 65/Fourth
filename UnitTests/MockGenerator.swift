//
//  MockGenerator.swift
//  UnitTests
//
//  Created by Ivan Tsaryov on 15/01/2018.
//  Copyright © 2018 Ivan Tsaryov. All rights reserved.
//

class MockGenerator {

    class func usernames() -> [String] {
        return [
            "Krevetka",
            "Antonio",
            "Qwerty-Fox",
            "ElFuego",
            "Jigolo"
        ]
    }
    
    class func emails() -> [String] {
        return [
            "k0m0k@list.ru",
            "joestalin@yandex.ru",
            "gs519@mail.ru",
            "krevetkaa@mail.ru",
            "heik@yandex.ru"
        ]
    }
}
