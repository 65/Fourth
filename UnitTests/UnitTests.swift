//
//  UnitTests.swift
//  UnitTests
//
//  Created by Ivan Tsaryov on 15/01/2018.
//  Copyright © 2018 Ivan Tsaryov. All rights reserved.
//

import XCTest


class UnitTests: XCTestCase {
    
    let validator = LoginValidator()
    
    var usernames: [String] = []
    var emails: [String] = []
    
    override func setUp() {
        super.setUp()
        
        if let usernames = strings(from: "/Users/ivan/Desktop/usernames.txt") {
            self.usernames = usernames
        } else {
            usernames = MockGenerator.usernames()
        }
        
        if let emails = strings(from: "/Users/ivan/Desktop/emails.txt") {
            self.emails = emails
        } else {
            emails = MockGenerator.emails()
        }
    }
    
    func testEmails() {
        for str in emails {
            XCTAssertTrue(validator.isLoginValid(str), "invalid email: \(str)")
        }
    }
    
    func testUsernames() {
        for str in usernames {
            XCTAssertTrue(validator.isLoginValid(str), "invalid username: \(str)")
        }
    }
    
    private func strings(from file: String) -> [String]? {
        guard let data = try? String(contentsOfFile: file, encoding: .utf8) else {
            return nil
        }
        return data.components(separatedBy: .newlines)
    }
}
